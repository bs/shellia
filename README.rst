::::::::::::::::::::::::::::::::::::::::::::::::
shellia -- library for interactive shell scripts
::::::::::::::::::::::::::::::::::::::::::::::::

.. contents:: **Table of Contents**

Short Description
=================
shellia is a library that allows to run shell scripts interactive and helps to
familiarize oneself with large shell scripts, find the cause of unexpected
behaviour in shell scripts, and run shell scripts silently, while checking
them step by step.

Why does it exist
=================
the author of this library, is working for more than 25 years in a large
consulting company. 
Only seldom there was enough time to build software in **C** or **Python**
for the customer.

For this reason **shell** is used very often, and also shell scripts can 
become very complex.
This leads to the well known problem https://en.wikipedia.org/wiki/Programming_complexity.

But in **shell** we can not easily use e.g. Object Oriented Design to reduce complexity.
In daily work this lead to many different **shell** libraries.
One is e.g implemented in the debian package https://packages.debian.org/stretch/bootcd.
But as explained before, there was never enough time to build something,
that put all good parts together, removed bad parts and added a 
separate documentation only for the library.

Hopefully this will change now, with **shellia**.

Quick Start
===========

Download, build and install::

  git clone https://salsa.debian.org/bs/shellia.git
  cd shellia
  make # or: cd src; debuild -- clean binary
  sudo make install # or: sudo dpkg -i shellia_*.deb

Read the docs and try example script::

  man 7 shellia
  man 3 shellia
  man 1 shellia
  /usr/share/doc/shellia/examples/hello_world -i

Dokumentation
=============

- shellia(1): https://salsa.debian.org/bs/shellia/blob/master/src/shellia-1.rst (execute *sripts.using.shellia*)
- shellia(3): https://salsa.debian.org/bs/shellia/blob/master/src/shellia-3.rst (develop **shell** with **ia** library)
- shellia(7): https://salsa.debian.org/bs/shellia/blob/master/src/shellia-7.rst (miscellaneous about **shellia**)

License
=======

- GPL 3: https://salsa.debian.org/bs/shellia/blob/master/src/debian/copyright
