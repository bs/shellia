#!/bin/sh
# vim: set filetype=sh :
#        file: /usr/share/shellia/ia
#   copyright: Bernd Schumacher <bernd.schumacher@hpe.com> (2007-2021)
#     license: GNU General Public License, version 3
# description: ia - load all shellia libraries

# ia_find_lib <lib>
# example:
#   programm "/nfs/host1/usr/local/bin/program" with line "ia_source_lib /usr/share/shellia/ia" called
#   from directory "/tmp" will try "/tmp/ia", ... "/nfs/host1/usr/share/shellia/ia", ... and
#   "/usr/share/shellia/ia" in the listed order.
ia_find_lib()
{
  local i
  local p

  for i in "$@"; do
    [ -f "./$(basename "$i")" ] && /bin/echo "./$(basename "$i")" && return 0
    p="$0"
    while :; do
      [ "$p" != "$(dirname p)" ] || break
      p="$(dirname "$p")"
      [ -f "$p/$i" ] && /bin/echo "$p/$i" && return 0
    done
    [ -f "$i" ] && /bin/echo "$i" && return 0
    return 1
  done
}

if [ "${ia_prefix:=ia}" != "ia" ] || [ "${dbg_prefix:=dbg}" != "dbg" ]; then
  [ -n "$(eval "/bin/echo \${${ia_prefix}_already_sourced:=}")" ] && return
  eval "$(sed -u -e "s/ia_/${ia_prefix}_/g" -e "s/ia()/${ia_prefix}()/g" \
    -e "s/dbg/${dbg_prefix}/g" "$(ia_find_lib /usr/share/shellia/ia)")"
  return
fi

[ -n "${ia_already_sourced:=}" ] && return || ia_already_sourced=1

. "$(ia_find_lib /usr/share/shellia/ia.basic)"
. "$(ia_find_lib /usr/share/shellia/ia.log)"
. "$(ia_find_lib /usr/share/shellia/ia.check)"
. "$(ia_find_lib /usr/share/shellia/ia.debug)"
. "$(ia_find_lib /usr/share/shellia/ia.interactive)"
. "$(ia_find_lib /usr/share/shellia/ia.highlevel)"
