#!/bin/bash
# vim: set filetype=sh :
#        file: example.ia-expr
#   copyright: Bernd Schumacher <bernd.schumacher@hpe.com> (2007-2020)
#     license: GNU General Public License, version 3
# description: example using expressions
#    see also: test.ia-expr
set -e
set -u

. ./ia

sub()
{
  eval "$ia_init"

  # AC"DC
  ia_add "echo \"AC\\\"DC\"" # [ "$(eval "echo \"AC\\\"DC\"")" = 'AC"DC' ]

  # dc="" AC$dc
  ia_add "dc=\"\""
  ia_add "echo \"AC\$dc\""

  # dc="DC" AC$dc
  ia_add "dc=\"DC\""
  ia_add "echo \"AC\$dc\""

  # ACDC$
  ia_add "echo \"ACDC$\""

  ia_add "cat <<'END'
ACDC$
END
"
  ia_add "echo \"ACDC$\""

  # AC\DC
  ia_add "echo \"AC\\\\DC\"" # [ "AC\\\\DC" = 'AC\\DC' ]
  ia_add "echo \"AC\\DC\"" # [ "AC\\DC" = 'AC\DC' ]
  ia_add "cat <<'END'
AC\\DC
END
"
  ia_add "echo 'AC\\DC'"
  ia_add "echo one; echo two; echo three"
  ia_add "echo one"
  ia_add "echo two"

  # number of backslashes 1, 3, 7, 15, 31, 63 ...
  ia_add "eval \"echo \\\"one\\\"\""
  ia_add "eval \"eval \\\"echo \\\\\\\"two\\\\\\\"\\\"\""
  ia_add "eval \"eval \\\"eval \\\\\\\"echo \\\\\\\\\\\\\\\"three\\\\\\\\\\\\\\\"\\\\\\\"\\\"\""
  ia_add "eval \"eval \\\"eval \\\\\\\"eval \\\\\\\\\\\\\\\"echo \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\"four\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\"\\\\\\\\\\\\\\\"\\\\\\\"\\\"\""

  ia
}

eval "$ia_init"
ia_add "sub <-i>"
ia
