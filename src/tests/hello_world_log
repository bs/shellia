#!/bin/dash
#        file: hello_world_log
#   copyright: Bernd Schumacher <bernd.schumacher@hpe.com> (2007-2020)
#     license: GNU General Public License, version 3
# description: example "hello world" script with logging
#     warning: Be aware that the interactive step size in this example is only
#              reasonable to summanrized demonstrate shellia features.
#              Read shellia(7) NOTES, to learn about a reasonable size of
#              interactive steps.
#       usage: ./hello_world_log [-d <level>] [-i|-s|-m] [--] <name>
#              Read shellia(1) logfile-mode, about running the examples
#              Read shellia(3) logfile-mode, about ia.log library
#              Example 1: ./hello_world_log -d 99 -- "shellia user"
#              Example 2: ./hello_world_log -i "shellia user" # and enter
#                         2<RETURN>, c<RETURN> and q<RETURN>

ia_logfile="$(mktemp)"
export ia_logfile

# we prefer a local copy of ia, if it exists
[ -f ia ] && . ./ia || . /usr/share/shellia/ia

# say_hello <name>
say_hello()
{
  local name
  eval "$ia_init"
  ia_add "dbg \"function say_hello called with $# arguments\""
  ia_add "name=\"$1\""
  ia_stdout "^hello"
  ia_add "echo \"hello \$name\""
  ia_add "dbg \"function say_hello end\""
  ia -c
}

eval "$ia_init"
ia_add "dbg \"main program begin\""
ia_nocheck # do not check the already checked output of next line
ia_add "say_hello <-i> -- \"$1\""
ia_add "dbg \"main program end\""
ia -c

# display the LOGFILE and then delete it
echo "--- LOGFILE ---"
cat $ia_logfile
echo "---------------"
rm $ia_logfile
