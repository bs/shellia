#!/bin/sh
# vim: set filetype=sh :
#        file: test.filefd
#   copyright: Bernd Schumacher <bernd.schumacher@hpe.com> (2007-2021)
#     license: GNU General Public License, version 3
# description: test using shell script functions
#    see also: example.func

# (1) Run without options
# (2) Run wih local log
# (3) check local logfile
# (4) Run remote wih local log
# (5) check local logfile
# (6) Run wih local and remote log
# (7) check local logfile
# (8) check remote logfile
# (9) Run interactive without options
# (10) Run interactive wih local log
# (11) check local logfile
# (12) Run interactive remote wih local log
# (13) check local logfile
# (14) Run interactive wih local and remote log
# (15) check local logfile
# (16) check remote logfile

. ./tstlib


# because the free fd can change fast, we can not precalculate them
UNIFY_freefd="sed -e \"s/ia_log_fd=<[4-9]>/ia_log_fd=<freefd>/g\""

shell=""
while [ $# -ne 0 ]; do
  if [ "$1" = "-s" ]; then
    shell="$2"
    shift 2
  else
    echo "ERROR $0: unknown option <$1>" >&2
    exit 1
  fi
done

set -- "$@" -u "$UNIFY_freefd"

# to use ia_file mksh needs -o posix
# because only then, as stated in mksh(1):
# File descriptors created by I/O redirections are inherited by child processes.
[ "$shell" = "mksh" ] && shell="mksh -o posix"

[ "$shell" ] && set -- "$@" -s "$shell"

cmd=$(dirname $0)/example.filefd
llog="$(mktemp)"
rlog="$(mktemp)"

check "(1) Run without options" -i "seq 7 | sed \"s/.*//\"" "$@" \
  "$cmd -a" \
"example_filefd-4: ia_logfile=</dev/null> ia_log_fd=<freefd>"

rm -f $llog
check "(2) Run wih local log" -i "seq 7 | sed \"s/.*//\"" "$@" \
  "$cmd -a --log $llog" \
"example_filefd-4: ia_logfile=<$llog> ia_log_fd=<freefd>"

check "(3) check local logfile" "$@" "cat $llog" \
"|example.filefd
|example_filefd-3: running own script
|example_filefd-4: ia_logfile=<$llog> ia_log_fd=<freefd>"

rm -f $llog
check "(4) Run remote wih local log" -i "seq 7 | sed \"s/.*//\"" "$@" \
  "$cmd -a --file --log $llog" \
"example_filefd-2: ia_logfile=<$llog> ia_log_fd=<freefd>
example_filefd-4: ia_logfile=<> ia_log_fd=<freefd>"

check "(5) check local logfile" "$@" "cat $llog" \
"|example.filefd
|example_filefd-1: will run separate file
|example_filefd-2: ia_logfile=<$llog> ia_log_fd=<freefd>
|example.filefd:example.filefd
|example_filefd-3: running own script
|example_filefd-4: ia_logfile=<> ia_log_fd=<freefd>"

rm -f $llog $rlog
check "(6) Run wih local and remote log" -i "seq 7 | sed \"s/.*//\"" "$@" \
  "$cmd -a --file --log $llog --filelog $rlog" \
"example_filefd-2: ia_logfile=<$llog> ia_log_fd=<freefd>
example_filefd-4: ia_logfile=<$rlog> ia_log_fd=<freefd>"

check "(7) check local logfile" "$@" "cat $llog" \
"|example.filefd
|example_filefd-1: will run separate file
|example_filefd-2: ia_logfile=<$llog> ia_log_fd=<freefd>"

check "(8) check remote logfile" "$@" "cat $rlog" \
"|example.filefd:example.filefd
|example_filefd-3: running own script
|example_filefd-4: ia_logfile=<$rlog> ia_log_fd=<freefd>"

check "(9) Run interactive without options" -i "seq 7 | sed \"s/.*//\"" "$@" \
  "$cmd -a -i" \
"=== example.filefd ===
1 ia_log \"example_filefd-3: running own script\"
2 ia_logerr \"example_filefd-4: ia_logfile=<$<ia_logfile>> ia_log_fd=<$<ia_log_fd>>\"
d ... change dbg: 0
c continue without questions
q quit
? [1] 
=== example.filefd ===
1 ia_log \"example_filefd-3: running own script\"
2 ia_logerr \"example_filefd-4: ia_logfile=<$<ia_logfile>> ia_log_fd=<$<ia_log_fd>>\"
d ... change dbg: 0
q quit
? [2] 
example_filefd-4: ia_logfile=</dev/null> ia_log_fd=<freefd>
=== example.filefd ===
1 ia_log \"example_filefd-3: running own script\"
2 ia_logerr \"example_filefd-4: ia_logfile=<$<ia_logfile>> ia_log_fd=<$<ia_log_fd>>\"
d ... change dbg: 0
q quit
? [q] "

rm -f $llog
check "(10) Run interactive wih local log" -i "seq 7 | sed \"s/.*//\"" "$@" \
  "$cmd -a -i --log $llog" \
"=== example.filefd ===
1 ia_log \"example_filefd-3: running own script\"
2 ia_logerr \"example_filefd-4: ia_logfile=<$<ia_logfile>> ia_log_fd=<$<ia_log_fd>>\"
d ... change dbg: 0
c continue without questions
q quit
? [1] 
=== example.filefd ===
1 ia_log \"example_filefd-3: running own script\"
2 ia_logerr \"example_filefd-4: ia_logfile=<$<ia_logfile>> ia_log_fd=<$<ia_log_fd>>\"
d ... change dbg: 0
q quit
? [2] 
example_filefd-4: ia_logfile=<$llog> ia_log_fd=<freefd>
=== example.filefd ===
1 ia_log \"example_filefd-3: running own script\"
2 ia_logerr \"example_filefd-4: ia_logfile=<$<ia_logfile>> ia_log_fd=<$<ia_log_fd>>\"
d ... change dbg: 0
q quit
? [q] "

check "(11) check local logfile" "$@" "cat $llog" \
"|=== example.filefd ===
|1 ia_log \"example_filefd-3: running own script\"
|2 ia_logerr \"example_filefd-4: ia_logfile=<$<ia_logfile>> ia_log_fd=<$<ia_log_fd>>\"
|d ... change dbg: 0
|c continue without questions
|q quit
|? [1] <RETURN>
|example_filefd-3: running own script
|=== example.filefd ===
|1 ia_log \"example_filefd-3: running own script\"
|2 ia_logerr \"example_filefd-4: ia_logfile=<$<ia_logfile>> ia_log_fd=<$<ia_log_fd>>\"
|d ... change dbg: 0
|q quit
|? [2] <RETURN>
|example_filefd-4: ia_logfile=<$llog> ia_log_fd=<freefd>
|=== example.filefd ===
|1 ia_log \"example_filefd-3: running own script\"
|2 ia_logerr \"example_filefd-4: ia_logfile=<$<ia_logfile>> ia_log_fd=<$<ia_log_fd>>\"
|d ... change dbg: 0
|q quit
|? [q] <RETURN>"

rm -f $llog
check "(12) Run interactive remote wih local log" -i "seq 14 | sed \"s/.*//\"" "$@" \
  "$cmd -a -i --file --log $llog" \
"=== example.filefd ===
1 ia_log \"example_filefd-1: will run separate file\"
2 ia_logerr \"example_filefd-2: ia_logfile=<$<ia_logfile>> ia_log_fd=<$<ia_log_fd>>\"
3 ia_file ./tests/example.filefd -i
d ... change dbg: 0
i toggle -i flag
c continue without questions
q quit
? [1] 
=== example.filefd ===
1 ia_log \"example_filefd-1: will run separate file\"
2 ia_logerr \"example_filefd-2: ia_logfile=<$<ia_logfile>> ia_log_fd=<$<ia_log_fd>>\"
3 ia_file ./tests/example.filefd -i
d ... change dbg: 0
i toggle -i flag
c continue without questions
q quit
? [2] 
example_filefd-2: ia_logfile=<$llog> ia_log_fd=<freefd>
=== example.filefd ===
1 ia_log \"example_filefd-1: will run separate file\"
2 ia_logerr \"example_filefd-2: ia_logfile=<$<ia_logfile>> ia_log_fd=<$<ia_log_fd>>\"
3 ia_file ./tests/example.filefd -i
d ... change dbg: 0
i toggle -i flag
q quit
? [3] 
=== example.filefd:example.filefd ===
1 ia_log \"example_filefd-3: running own script\"
2 ia_logerr \"example_filefd-4: ia_logfile=<$<ia_logfile>> ia_log_fd=<$<ia_log_fd>>\"
d ... change dbg: 0
c continue without questions
q quit
? [1] 
=== example.filefd:example.filefd ===
1 ia_log \"example_filefd-3: running own script\"
2 ia_logerr \"example_filefd-4: ia_logfile=<$<ia_logfile>> ia_log_fd=<$<ia_log_fd>>\"
d ... change dbg: 0
q quit
? [2] 
example_filefd-4: ia_logfile=<> ia_log_fd=<freefd>
=== example.filefd:example.filefd ===
1 ia_log \"example_filefd-3: running own script\"
2 ia_logerr \"example_filefd-4: ia_logfile=<$<ia_logfile>> ia_log_fd=<$<ia_log_fd>>\"
d ... change dbg: 0
q quit
? [q] 
=== example.filefd ===
1 ia_log \"example_filefd-1: will run separate file\"
2 ia_logerr \"example_filefd-2: ia_logfile=<$<ia_logfile>> ia_log_fd=<$<ia_log_fd>>\"
3 ia_file ./tests/example.filefd -i
d ... change dbg: 0
i toggle -i flag
q quit
? [q] "

check "(13) check local logfile" "$@" "cat $llog" \
"|=== example.filefd ===
|1 ia_log \"example_filefd-1: will run separate file\"
|2 ia_logerr \"example_filefd-2: ia_logfile=<$<ia_logfile>> ia_log_fd=<$<ia_log_fd>>\"
|3 ia_file ./tests/example.filefd -i
|d ... change dbg: 0
|i toggle -i flag
|c continue without questions
|q quit
|? [1] <RETURN>
|example_filefd-1: will run separate file
|=== example.filefd ===
|1 ia_log \"example_filefd-1: will run separate file\"
|2 ia_logerr \"example_filefd-2: ia_logfile=<$<ia_logfile>> ia_log_fd=<$<ia_log_fd>>\"
|3 ia_file ./tests/example.filefd -i
|d ... change dbg: 0
|i toggle -i flag
|c continue without questions
|q quit
|? [2] <RETURN>
|example_filefd-2: ia_logfile=<$llog> ia_log_fd=<freefd>
|=== example.filefd ===
|1 ia_log \"example_filefd-1: will run separate file\"
|2 ia_logerr \"example_filefd-2: ia_logfile=<$<ia_logfile>> ia_log_fd=<$<ia_log_fd>>\"
|3 ia_file ./tests/example.filefd -i
|d ... change dbg: 0
|i toggle -i flag
|q quit
|? [3] <RETURN>
|=== example.filefd:example.filefd ===
|1 ia_log \"example_filefd-3: running own script\"
|2 ia_logerr \"example_filefd-4: ia_logfile=<$<ia_logfile>> ia_log_fd=<$<ia_log_fd>>\"
|d ... change dbg: 0
|c continue without questions
|q quit
|? [1] <RETURN>
|example_filefd-3: running own script
|=== example.filefd:example.filefd ===
|1 ia_log \"example_filefd-3: running own script\"
|2 ia_logerr \"example_filefd-4: ia_logfile=<$<ia_logfile>> ia_log_fd=<$<ia_log_fd>>\"
|d ... change dbg: 0
|q quit
|? [2] <RETURN>
|example_filefd-4: ia_logfile=<> ia_log_fd=<freefd>
|=== example.filefd:example.filefd ===
|1 ia_log \"example_filefd-3: running own script\"
|2 ia_logerr \"example_filefd-4: ia_logfile=<$<ia_logfile>> ia_log_fd=<$<ia_log_fd>>\"
|d ... change dbg: 0
|q quit
|? [q] <RETURN>
|=== example.filefd ===
|1 ia_log \"example_filefd-1: will run separate file\"
|2 ia_logerr \"example_filefd-2: ia_logfile=<$<ia_logfile>> ia_log_fd=<$<ia_log_fd>>\"
|3 ia_file ./tests/example.filefd -i
|d ... change dbg: 0
|i toggle -i flag
|q quit
|? [q] <RETURN>"

rm -f $llog $rlog
check "(14) Run interactive wih local and remote log" -i "seq 7 | sed \"s/.*//\"" "$@" \
  "$cmd -a -i --file --log $llog  --filelog $rlog" \
"=== example.filefd ===
1 ia_log \"example_filefd-1: will run separate file\"
2 ia_logerr \"example_filefd-2: ia_logfile=<$<ia_logfile>> ia_log_fd=<$<ia_log_fd>>\"
3 ia_file ./tests/example.filefd -i --log $rlog
d ... change dbg: 0
i toggle -i flag
c continue without questions
q quit
? [1] 
=== example.filefd ===
1 ia_log \"example_filefd-1: will run separate file\"
2 ia_logerr \"example_filefd-2: ia_logfile=<$<ia_logfile>> ia_log_fd=<$<ia_log_fd>>\"
3 ia_file ./tests/example.filefd -i --log $rlog
d ... change dbg: 0
i toggle -i flag
c continue without questions
q quit
? [2] 
example_filefd-2: ia_logfile=<$llog> ia_log_fd=<freefd>
=== example.filefd ===
1 ia_log \"example_filefd-1: will run separate file\"
2 ia_logerr \"example_filefd-2: ia_logfile=<$<ia_logfile>> ia_log_fd=<$<ia_log_fd>>\"
3 ia_file ./tests/example.filefd -i --log $rlog
d ... change dbg: 0
i toggle -i flag
q quit
? [3] 
=== example.filefd:example.filefd ===
1 ia_log \"example_filefd-3: running own script\"
2 ia_logerr \"example_filefd-4: ia_logfile=<$<ia_logfile>> ia_log_fd=<$<ia_log_fd>>\"
d ... change dbg: 0
c continue without questions
q quit
? [1] 
=== example.filefd:example.filefd ===
1 ia_log \"example_filefd-3: running own script\"
2 ia_logerr \"example_filefd-4: ia_logfile=<$<ia_logfile>> ia_log_fd=<$<ia_log_fd>>\"
d ... change dbg: 0
q quit
? [2] 
example_filefd-4: ia_logfile=<$rlog> ia_log_fd=<freefd>
=== example.filefd:example.filefd ===
1 ia_log \"example_filefd-3: running own script\"
2 ia_logerr \"example_filefd-4: ia_logfile=<$<ia_logfile>> ia_log_fd=<$<ia_log_fd>>\"
d ... change dbg: 0
q quit
? [q] 
=== example.filefd ===
1 ia_log \"example_filefd-1: will run separate file\"
2 ia_logerr \"example_filefd-2: ia_logfile=<$<ia_logfile>> ia_log_fd=<$<ia_log_fd>>\"
3 ia_file ./tests/example.filefd -i --log $rlog
d ... change dbg: 0
i toggle -i flag
q quit
? [q] "

check "(15) check local logfile" "$@" "cat $llog" \
"|=== example.filefd ===
|1 ia_log \"example_filefd-1: will run separate file\"
|2 ia_logerr \"example_filefd-2: ia_logfile=<$<ia_logfile>> ia_log_fd=<$<ia_log_fd>>\"
|3 ia_file ./tests/example.filefd -i --log $rlog
|d ... change dbg: 0
|i toggle -i flag
|c continue without questions
|q quit
|? [1] <RETURN>
|example_filefd-1: will run separate file
|=== example.filefd ===
|1 ia_log \"example_filefd-1: will run separate file\"
|2 ia_logerr \"example_filefd-2: ia_logfile=<$<ia_logfile>> ia_log_fd=<$<ia_log_fd>>\"
|3 ia_file ./tests/example.filefd -i --log $rlog
|d ... change dbg: 0
|i toggle -i flag
|c continue without questions
|q quit
|? [2] <RETURN>
|example_filefd-2: ia_logfile=<$llog> ia_log_fd=<freefd>
|=== example.filefd ===
|1 ia_log \"example_filefd-1: will run separate file\"
|2 ia_logerr \"example_filefd-2: ia_logfile=<$<ia_logfile>> ia_log_fd=<$<ia_log_fd>>\"
|3 ia_file ./tests/example.filefd -i --log $rlog
|d ... change dbg: 0
|i toggle -i flag
|q quit
|? [3] <RETURN>
|=== example.filefd ===
|1 ia_log \"example_filefd-1: will run separate file\"
|2 ia_logerr \"example_filefd-2: ia_logfile=<$<ia_logfile>> ia_log_fd=<$<ia_log_fd>>\"
|3 ia_file ./tests/example.filefd -i --log $rlog
|d ... change dbg: 0
|i toggle -i flag
|q quit
|? [q] <RETURN>"

check "(16) check remote logfile" "$@" "cat $rlog" \
"|=== example.filefd:example.filefd ===
|1 ia_log \"example_filefd-3: running own script\"
|2 ia_logerr \"example_filefd-4: ia_logfile=<$<ia_logfile>> ia_log_fd=<$<ia_log_fd>>\"
|d ... change dbg: 0
|c continue without questions
|q quit
|? [1] <RETURN>
|example_filefd-3: running own script
|=== example.filefd:example.filefd ===
|1 ia_log \"example_filefd-3: running own script\"
|2 ia_logerr \"example_filefd-4: ia_logfile=<$<ia_logfile>> ia_log_fd=<$<ia_log_fd>>\"
|d ... change dbg: 0
|q quit
|? [2] <RETURN>
|example_filefd-4: ia_logfile=<$rlog> ia_log_fd=<freefd>
|=== example.filefd:example.filefd ===
|1 ia_log \"example_filefd-3: running own script\"
|2 ia_logerr \"example_filefd-4: ia_logfile=<$<ia_logfile>> ia_log_fd=<$<ia_log_fd>>\"
|d ... change dbg: 0
|q quit
|? [q] <RETURN>"

rm -f $llog $rlog
