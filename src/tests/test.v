#!/bin/sh
# vim: set filetype=sh :
#        file: test.v
#   copyright: Bernd Schumacher <bernd.schumacher@hpe.com> (2007-2022)
#     license: GNU General Public License, version 3
# description: check pressing of v to show \${variable}
#    see also: example.v

# The following tests are included:
# (1) Run with -i
# (2) check logfile

. ./tstlib

shell=""
while [ $# -ne 0 ]; do
  if [ "$1" = "-s" ]; then
    shell="$2"
    shift 2
  else
    /bin/echo "ERROR $0: unknown option <$1>" >&2
    exit 1
  fi
done

[ "$shell" ] && set -- "$@" -s "$shell"
bug913718_unify UNIFY_posh
[ "$shell" = "posh" ] && set -- "$@" -u "$UNIFY_posh"

cmd=$(dirname $0)/example.v
bug913718_cmd

ia_logfile="$(mktemp)"
export ia_logfile

rm -f $ia_logfile
check "(1) Run with -i" -i "/bin/echo \"1
v
2
3
4
5
6
7
8
q
\"" "$@" "$cmd -a -i" \
"=== example.v ===
1 xyz=39
2 xyz=\$(( $<xyz> + 1 ))
3 xyz=\$(( $<xyz> + 1 ))
4 xyz=\$(( $<xyz> + 1 ))
5 /bin/echo \"xyz=$<xyz>\"
6 /bin/echo \"xyz=$<xyz>meter backslash=\\\\\\\\ quote=\\\" dollar=\\\$ dollarX=\\\$X\"
7 a=\"
---
\"
8 /bin/echo \"prg \\\"$<a>\\\"\"
d ... change dbg: 0
c continue without questions
q quit
? [1] 
=== example.v ===
1 xyz=39
2 xyz=\$(( $<xyz> + 1 ))
3 xyz=\$(( $<xyz> + 1 ))
4 xyz=\$(( $<xyz> + 1 ))
5 /bin/echo \"xyz=$<xyz>\"
6 /bin/echo \"xyz=$<xyz>meter backslash=\\\\\\\\ quote=\\\" dollar=\\\$ dollarX=\\\$X\"
7 a=\"
---
\"
8 /bin/echo \"prg \\\"$<a>\\\"\"
d ... change dbg: 0
c continue without questions
q quit
? [2] 
=== example.v ===
1 xyz=39
2 xyz=\$(( 39 + 1 ))
3 xyz=\$(( 39 + 1 ))
4 xyz=\$(( 39 + 1 ))
5 /bin/echo \"xyz=39\"
6 /bin/echo \"xyz=39meter backslash=\\\\\\\\ quote=\\\" dollar=\\\$ dollarX=\\\$X\"
7 a=\"
---
\"
8 /bin/echo \"prg \\\"\\\"\"
d ... change dbg: 0
c continue without questions
q quit
? [2] 
=== example.v ===
1 xyz=39
2 xyz=\$(( 40 + 1 ))
3 xyz=\$(( 40 + 1 ))
4 xyz=\$(( 40 + 1 ))
5 /bin/echo \"xyz=40\"
6 /bin/echo \"xyz=40meter backslash=\\\\\\\\ quote=\\\" dollar=\\\$ dollarX=\\\$X\"
7 a=\"
---
\"
8 /bin/echo \"prg \\\"\\\"\"
d ... change dbg: 0
c continue without questions
q quit
? [3] 
=== example.v ===
1 xyz=39
2 xyz=\$(( 41 + 1 ))
3 xyz=\$(( 41 + 1 ))
4 xyz=\$(( 41 + 1 ))
5 /bin/echo \"xyz=41\"
6 /bin/echo \"xyz=41meter backslash=\\\\\\\\ quote=\\\" dollar=\\\$ dollarX=\\\$X\"
7 a=\"
---
\"
8 /bin/echo \"prg \\\"\\\"\"
d ... change dbg: 0
c continue without questions
q quit
? [4] 
=== example.v ===
1 xyz=39
2 xyz=\$(( 42 + 1 ))
3 xyz=\$(( 42 + 1 ))
4 xyz=\$(( 42 + 1 ))
5 /bin/echo \"xyz=42\"
6 /bin/echo \"xyz=42meter backslash=\\\\\\\\ quote=\\\" dollar=\\\$ dollarX=\\\$X\"
7 a=\"
---
\"
8 /bin/echo \"prg \\\"\\\"\"
d ... change dbg: 0
c continue without questions
q quit
? [5] 
xyz=42
=== example.v ===
1 xyz=39
2 xyz=\$(( 42 + 1 ))
3 xyz=\$(( 42 + 1 ))
4 xyz=\$(( 42 + 1 ))
5 /bin/echo \"xyz=42\"
6 /bin/echo \"xyz=42meter backslash=\\\\\\\\ quote=\\\" dollar=\\\$ dollarX=\\\$X\"
7 a=\"
---
\"
8 /bin/echo \"prg \\\"\\\"\"
d ... change dbg: 0
c continue without questions
q quit
? [6] 
xyz=42meter backslash=\\\\ quote=\" dollar=\$ dollarX=\$X
=== example.v ===
1 xyz=39
2 xyz=\$(( 42 + 1 ))
3 xyz=\$(( 42 + 1 ))
4 xyz=\$(( 42 + 1 ))
5 /bin/echo \"xyz=42\"
6 /bin/echo \"xyz=42meter backslash=\\\\\\\\ quote=\\\" dollar=\\\$ dollarX=\\\$X\"
7 a=\"
---
\"
8 /bin/echo \"prg \\\"\\\"\"
d ... change dbg: 0
c continue without questions
q quit
? [7] 
=== example.v ===
1 xyz=39
2 xyz=\$(( 42 + 1 ))
3 xyz=\$(( 42 + 1 ))
4 xyz=\$(( 42 + 1 ))
5 /bin/echo \"xyz=42\"
6 /bin/echo \"xyz=42meter backslash=\\\\\\\\ quote=\\\" dollar=\\\$ dollarX=\\\$X\"
7 a=\"
---
\"
8 /bin/echo \"prg \\\"
---\\\"\"
d ... change dbg: 0
q quit
? [8] 
prg \"
---\"
=== example.v ===
1 xyz=39
2 xyz=\$(( 42 + 1 ))
3 xyz=\$(( 42 + 1 ))
4 xyz=\$(( 42 + 1 ))
5 /bin/echo \"xyz=42\"
6 /bin/echo \"xyz=42meter backslash=\\\\\\\\ quote=\\\" dollar=\\\$ dollarX=\\\$X\"
7 a=\"
---
\"
8 /bin/echo \"prg \\\"
---\\\"\"
d ... change dbg: 0
q quit
? [q] "

check "(2) check logfile" "$@" "cat $ia_logfile" \
"|=== example.v ===
|1 xyz=39
|2 xyz=\$(( $<xyz> + 1 ))
|3 xyz=\$(( $<xyz> + 1 ))
|4 xyz=\$(( $<xyz> + 1 ))
|5 /bin/echo \"xyz=$<xyz>\"
|6 /bin/echo \"xyz=$<xyz>meter backslash=\\\\\\\\ quote=\\\" dollar=\\\$ dollarX=\\\$X\"
|7 a=\"
|---
|\"
|8 /bin/echo \"prg \\\"$<a>\\\"\"
|d ... change dbg: 0
|c continue without questions
|q quit
|? [1] 1<RETURN>
|=== example.v ===
|1 xyz=39
|2 xyz=\$(( $<xyz> + 1 ))
|3 xyz=\$(( $<xyz> + 1 ))
|4 xyz=\$(( $<xyz> + 1 ))
|5 /bin/echo \"xyz=$<xyz>\"
|6 /bin/echo \"xyz=$<xyz>meter backslash=\\\\\\\\ quote=\\\" dollar=\\\$ dollarX=\\\$X\"
|7 a=\"
|---
|\"
|8 /bin/echo \"prg \\\"$<a>\\\"\"
|d ... change dbg: 0
|c continue without questions
|q quit
|? [2] v<RETURN>
|=== example.v ===
|1 xyz=39
|2 xyz=\$(( 39 + 1 ))
|3 xyz=\$(( 39 + 1 ))
|4 xyz=\$(( 39 + 1 ))
|5 /bin/echo \"xyz=39\"
|6 /bin/echo \"xyz=39meter backslash=\\\\\\\\ quote=\\\" dollar=\\\$ dollarX=\\\$X\"
|7 a=\"
|---
|\"
|8 /bin/echo \"prg \\\"\\\"\"
|d ... change dbg: 0
|c continue without questions
|q quit
|? [2] 2<RETURN>
|=== example.v ===
|1 xyz=39
|2 xyz=\$(( 40 + 1 ))
|3 xyz=\$(( 40 + 1 ))
|4 xyz=\$(( 40 + 1 ))
|5 /bin/echo \"xyz=40\"
|6 /bin/echo \"xyz=40meter backslash=\\\\\\\\ quote=\\\" dollar=\\\$ dollarX=\\\$X\"
|7 a=\"
|---
|\"
|8 /bin/echo \"prg \\\"\\\"\"
|d ... change dbg: 0
|c continue without questions
|q quit
|? [3] 3<RETURN>
|=== example.v ===
|1 xyz=39
|2 xyz=\$(( 41 + 1 ))
|3 xyz=\$(( 41 + 1 ))
|4 xyz=\$(( 41 + 1 ))
|5 /bin/echo \"xyz=41\"
|6 /bin/echo \"xyz=41meter backslash=\\\\\\\\ quote=\\\" dollar=\\\$ dollarX=\\\$X\"
|7 a=\"
|---
|\"
|8 /bin/echo \"prg \\\"\\\"\"
|d ... change dbg: 0
|c continue without questions
|q quit
|? [4] 4<RETURN>
|=== example.v ===
|1 xyz=39
|2 xyz=\$(( 42 + 1 ))
|3 xyz=\$(( 42 + 1 ))
|4 xyz=\$(( 42 + 1 ))
|5 /bin/echo \"xyz=42\"
|6 /bin/echo \"xyz=42meter backslash=\\\\\\\\ quote=\\\" dollar=\\\$ dollarX=\\\$X\"
|7 a=\"
|---
|\"
|8 /bin/echo \"prg \\\"\\\"\"
|d ... change dbg: 0
|c continue without questions
|q quit
|? [5] 5<RETURN>
|=== example.v ===
|1 xyz=39
|2 xyz=\$(( 42 + 1 ))
|3 xyz=\$(( 42 + 1 ))
|4 xyz=\$(( 42 + 1 ))
|5 /bin/echo \"xyz=42\"
|6 /bin/echo \"xyz=42meter backslash=\\\\\\\\ quote=\\\" dollar=\\\$ dollarX=\\\$X\"
|7 a=\"
|---
|\"
|8 /bin/echo \"prg \\\"\\\"\"
|d ... change dbg: 0
|c continue without questions
|q quit
|? [6] 6<RETURN>
|=== example.v ===
|1 xyz=39
|2 xyz=\$(( 42 + 1 ))
|3 xyz=\$(( 42 + 1 ))
|4 xyz=\$(( 42 + 1 ))
|5 /bin/echo \"xyz=42\"
|6 /bin/echo \"xyz=42meter backslash=\\\\\\\\ quote=\\\" dollar=\\\$ dollarX=\\\$X\"
|7 a=\"
|---
|\"
|8 /bin/echo \"prg \\\"\\\"\"
|d ... change dbg: 0
|c continue without questions
|q quit
|? [7] 7<RETURN>
|=== example.v ===
|1 xyz=39
|2 xyz=\$(( 42 + 1 ))
|3 xyz=\$(( 42 + 1 ))
|4 xyz=\$(( 42 + 1 ))
|5 /bin/echo \"xyz=42\"
|6 /bin/echo \"xyz=42meter backslash=\\\\\\\\ quote=\\\" dollar=\\\$ dollarX=\\\$X\"
|7 a=\"
|---
|\"
|8 /bin/echo \"prg \\\"
|---\\\"\"
|d ... change dbg: 0
|q quit
|? [8] 8<RETURN>
|=== example.v ===
|1 xyz=39
|2 xyz=\$(( 42 + 1 ))
|3 xyz=\$(( 42 + 1 ))
|4 xyz=\$(( 42 + 1 ))
|5 /bin/echo \"xyz=42\"
|6 /bin/echo \"xyz=42meter backslash=\\\\\\\\ quote=\\\" dollar=\\\$ dollarX=\\\$X\"
|7 a=\"
|---
|\"
|8 /bin/echo \"prg \\\"
|---\\\"\"
|d ... change dbg: 0
|q quit
|? [q] q<RETURN>"

rm -f $ia_logfile
