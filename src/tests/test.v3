#!/bin/sh
# vim: set filetype=sh :
#        file: test.v3
#   copyright: Bernd Schumacher <bernd.schumacher@hpe.com> (2007-2021)
#     license: GNU General Public License, version 3
# description: check pressing of v to show \${variable}
#              this file checks 2 variables
#    see also: example.v3

# The following tests are included:
# (1) Run with -i
# (2) check logfile

. ./tstlib

shell=""
while [ $# -ne 0 ]; do
  if [ "$1" = "-s" ]; then
    shell="$2"
    shift 2
  else
    /bin/echo "ERROR $0: unknown option <$1>" >&2
    exit 1
  fi
done

[ "$shell" ] && set -- "$@" -s "$shell"
cmd=$(dirname $0)/example.v3

ia_logfile="$(mktemp)"
export ia_logfile

rm -f $ia_logfile
check "(1) Run with -i" -i "/bin/echo \"v
1
q
\"" "$@" "$cmd -a -i" \
"=== example.v3 ===
1 echo \"a=<$<a>> b=<$<b>>\"
d ... change dbg: 0
q quit
? [1] 
=== example.v3 ===
1 echo \"a=<a a a> b=<b b b>\"
d ... change dbg: 0
q quit
? [1] 
a=<a a a> b=<b b b>
=== example.v3 ===
1 echo \"a=<a a a> b=<b b b>\"
d ... change dbg: 0
q quit
? [q] "

check "(2) check logfile" "$@" "cat $ia_logfile" \
"|=== example.v3 ===
|1 echo \"a=<$<a>> b=<$<b>>\"
|d ... change dbg: 0
|q quit
|? [1] v<RETURN>
|=== example.v3 ===
|1 echo \"a=<a a a> b=<b b b>\"
|d ... change dbg: 0
|q quit
|? [1] 1<RETURN>
|=== example.v3 ===
|1 echo \"a=<a a a> b=<b b b>\"
|d ... change dbg: 0
|q quit
|? [q] q<RETURN>"

rm -f $ia_logfile
