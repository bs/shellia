#!/bin/sh
# vim: set filetype=sh :
#        file: test.return
#   copyright: Bernd Schumacher <bernd.schumacher@hpe.com> (2007-2021)
#     license: GNU General Public License, version 3
# description: test handling of error output
#    see also: example.return

# The following tests are included:
# (1) Run this script without options
# (2) check logfile
# (3) Run this script trace enabled without options
# (4) check logfile
# (5) check output without_shellia
# (6) check logfile
# (7) Run this script with set -e and without options
# (8) check logfile

. ./tstlib

quote="'"
shell=""
while [ $# -ne 0 ]; do
  if [ "$1" = "-s" ]; then
    shell="$2"
    shift 2
  else
    echo "ERROR $0: unknown option <$1>" >&2
    exit 1
  fi
done

[ "$shell" = "bash" ] && set -- "$@" -u "$UNIFY_bash"
bug913718_unify UNIFY_posh
[ "$shell" = "posh" ] && set -- "$@" -u "$UNIFY_posh"
[ "$shell" = "mksh" ] && set -- "$@" -u "$UNIFY_mksh"
[ "$shell" = "busybox sh" ] && set -- "$@" -u "$UNIFY_busybox"

# TODO wenn shell = mksh erst tmp example.return.tmp.mksh erstellen ohne set -e

[ "$shell" ] && set -- "$@" -s "$shell"
cmd=$(dirname $0)/example.return
bug913718_cmd

ia_logfile="$(mktemp)"
export ia_logfile

rm -f $ia_logfile
check "(1) Run this script without options" "$@" "$cmd" "return=<0>
msg1 on stdout
msg2 on stderr
exit=42
return=<42>
msg1 on stdout
msg2 on stderr
return=<24>"

check "(2) check logfile" "$@" "cat $ia_logfile" "|example.return
|s:return=<0>
|s:msg1 on stdout
|s:msg2 on stderr
|s:exit=42
|s:return=<42>
|s:return=<24>"

cmd=$(dirname $0)/example.return.tmp.trace
cat $(dirname $0)/example.return | sed -e "s/^ia\>.*/ia -c -x/" > $cmd
chmod 755 $cmd
bug913718_cmd

rm -f $ia_logfile
check "(3) Run this script trace enabled without options" "$@" "$cmd" "+ :
+ echo return=<0>
return=<0>
+ fun1 42
+ echo msg1 on stdout
+ echo msg2 on stderr
+ return 42
msg1 on stdout
msg2 on stderr
exit=42
+ echo return=<42>
return=<42>
+ fun1 24
+ echo msg1 on stdout
msg1 on stdout
+ echo msg2 on stderr
msg2 on stderr
+ return 24
+ echo return=<24>
return=<24>"

check "(4) check logfile" "$@" "cat $ia_logfile" "|example.return.tmp.trace
|+ :
|+ echo return=<0>
|s:return=<0>
|+ fun1 42
|+ echo msg1 on stdout
|s:msg1 on stdout
|+ echo msg2 on stderr
|s:msg2 on stderr
|+ return 42
|s:exit=42
|+ echo return=<42>
|s:return=<42>
|+ echo return=<24>
|s:return=<24>"

cmd=$(dirname $0)/example.return.tmp.without_shellia
cat $(dirname $0)/example.return | sed -e "/^\. \.\/ia$/ d" |
  sed -e "/^eval \"\$ia_init\"$/ d" | sed -e "s/ia_add/eval/" -e "/^ia/d" > $cmd

chmod 755 $cmd
bug913718_cmd

rm -f $ia_logfile
check "(5) check output without_shellia" "$@" "$cmd" "return=<0>
msg1 on stdout
msg2 on stderr
return=<42>
msg1 on stdout
msg2 on stderr
return=<24>"

# witout shellia, we do not have a logfile
check "(6) check logfile" "$@" "cat $ia_logfile" "cat: $ia_logfile: No such file or directory
Command exited with non-zero status 1"

cmd=$(dirname $0)/example.return.tmp.set-e
cat $(dirname $0)/example.return | sed -e "s/^#set -e.*/set -e/" > $cmd
chmod 755 $cmd
bug913718_cmd
check "(7) Run this script with set -e and without options" "$@" "$cmd; echo \$?" \
"return=<0>
msg1 on stdout
msg2 on stderr
exit=42
return=<42>
msg1 on stdout
msg2 on stderr
24"

check "(8) check logfile" "$@" "cat $ia_logfile" \
"|example.return.tmp.set-e
|s:return=<0>
|s:msg1 on stdout
|s:msg2 on stderr
|s:exit=42
|s:return=<42>"

rm -f $ia_logfile
