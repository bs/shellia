#!/bin/sh
# vim: set filetype=sh :
#        file: test.return
#   copyright: Bernd Schumacher <bernd.schumacher@hpe.com> (2007-2020)
#     license: GNU General Public License, version 3
# description: test handling of error output
#    see also: example.return

# The following tests are included:
# (1) Run this script without options
# (2) check logfile

. ./tstlib

quote="'"
shell=""
while [ $# -ne 0 ]; do
  if [ "$1" = "-s" ]; then
    shell="$2"
    shift 2
  else
    echo "ERROR $0: unknown option <$1>" >&2
    exit 1
  fi
done

[ "$shell" = "bash" ] && set -- "$@" -u "$UNIFY_bash"
bug913718_unify UNIFY_posh
[ "$shell" = "posh" ] && set -- "$@" -u "$UNIFY_posh"
[ "$shell" = "mksh" ] && set -- "$@" -u "$UNIFY_mksh"
[ "$shell" = "busybox sh" ] && set -- "$@" -u "$UNIFY_busybox"

[ "$shell" ] && set -- "$@" -s "$shell"
cmd=$(dirname $0)/example.return2
bug913718_cmd

ia_logfile="$(mktemp)"
export ia_logfile

rm -f $ia_logfile
check "(1) Run this script without options" "$@" "$cmd" \
"=== no_ia ===
msg1 on stdout
msg2 on stderr
?=42
msg1 on stdout
msg2 on stderr
exit=43
?=43
msg1 on stdout
msg2 on stderr
?=44
=== ia_stdout ia -c ===
msg1 on stdout
msg2 on stderr
exit=42
msg1 on stdout
msg2 on stderr
exit=43
exit=43
msg1 on stdout
msg2 on stderr
exit=44
msg1 on stdout
exit=45
msg1 on stdout
msg2 on stderr
exit=46
?=46
=== ia_nocheck ia -c ===
msg1 on stdout
msg2 on stderr
msg1 on stdout
msg2 on stderr
exit=43
msg1 on stdout
msg2 on stderr
msg1 on stdout
msg1 on stdout
msg2 on stderr
?=46
=== ia_ignore ia -C ===
msg1 on stdout
msg1 on stdout
msg2 on stderr
exit=43
msg1 on stdout
msg1 on stdout
msg1 on stdout
?=46
=== ia_nocheck ia -C ===
msg1 on stdout
msg2 on stderr
msg1 on stdout
msg2 on stderr
exit=43
msg1 on stdout
msg2 on stderr
msg1 on stdout
msg1 on stdout
msg2 on stderr
?=46"

check "(2) check logfile" "$@" "cat $ia_logfile" \
"|example.return2
|s:msg1 on stdout
|s:msg2 on stderr
|s:exit=43
||example.return2/return
|example.return2
|s:msg1 on stdout
|s:msg2 on stderr
|s:exit=42
||example.return2/fun_ia_c
||s:msg1 on stdout
||s:msg2 on stderr
||s:exit=43
|s:msg1 on stdout
|s:msg2 on stderr
|s:exit=43
|s:exit=43
||example.return2/fun_ia_c_nocheck
|s:msg1 on stdout
|s:msg2 on stderr
|s:exit=44
||example.return2/fun_ia_C
||s:msg1 on stdout
||i:msg2 on stderr
||i:exit=45
|s:msg1 on stdout
|s:exit=45
||example.return2/fun_ia_C_nocheck
|s:msg1 on stdout
|s:msg2 on stderr
|s:exit=46
|example.return2
||example.return2/fun_ia_c
||s:msg1 on stdout
||s:msg2 on stderr
||s:exit=43
||example.return2/fun_ia_c_nocheck
||example.return2/fun_ia_C
||s:msg1 on stdout
||i:msg2 on stderr
||i:exit=45
||example.return2/fun_ia_C_nocheck
|example.return2
|i:msg2 on stderr
|i:exit=42
|s:msg1 on stdout
||example.return2/fun_ia_c
||s:msg1 on stdout
||s:msg2 on stderr
||s:exit=43
|i:exit=43
|s:msg1 on stdout
|s:msg2 on stderr
|s:exit=43
||example.return2/fun_ia_c_nocheck
|i:msg2 on stderr
|i:exit=44
|s:msg1 on stdout
||example.return2/fun_ia_C
||s:msg1 on stdout
||i:msg2 on stderr
||i:exit=45
|i:exit=45
|s:msg1 on stdout
||example.return2/fun_ia_C_nocheck
|i:msg2 on stderr
|i:exit=46
|s:msg1 on stdout
|example.return2
||example.return2/fun_ia_c
||s:msg1 on stdout
||s:msg2 on stderr
||s:exit=43
||example.return2/fun_ia_c_nocheck
||example.return2/fun_ia_C
||s:msg1 on stdout
||i:msg2 on stderr
||i:exit=45
||example.return2/fun_ia_C_nocheck"

rm -f $ia_logfile
