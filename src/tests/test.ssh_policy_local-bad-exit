#!/bin/sh
# vim: set filetype=sh :
#        file: test.ssh_policy_local-bad-exit
#   copyright: Bernd Schumacher <bernd.schumacher@hpe.com> (2007-2023)
#     license: GNU General Public License, version 3
# description: test 4 policies with exit != 0 on local host
#              policy-A: --check-mode-c and --policy-stop
#              policy-B: --check-mode-c and --policy-continue
#              policy-C: --check-mode-C and --policy-stop
#              policy-D: --check-mode-C and --policy-continue
#    see also: example.ssh_policy and symlink example.ssh_policy_remote

# The following tests are included:
# (1) check ..-c ..-stop ..-local-bad-exit
# (2) check ..-c ..-continue ..-local-bad-exit
# (3) check ..-C ..-stop ..-local-bad-exit
# (4) check ..-C ..-continue ..-local-bad-exit

. ./tstlib

if ./sshd2222 should-work; then
  sshd2222opts="$(./sshd2222 options)"
fi
export testdir="$(realpath .)"

shell=""
while [ $# -ne 0 ]; do
  if [ "$1" = "-s" ]; then
    shell="$2"
    shift 2
  else
    echo "ERROR $0: unknown option <$1>" >&2
    exit 1
  fi
done

[ "$shell" ] && set -- "$@" -s "$shell"
cmd="$(dirname $0)/example.ssh_policy"

ia_logfile="$(mktemp)"
export ia_logfile

res=\
"ERROR: ia premature exit of <exit 12>
12
|example.ssh_policy
|example.ssh_policy: start message
||example.ssh_policy/localhost:example.ssh_policy_remote
||example.ssh_policy_remote: start message
||example.ssh_policy_remote: end message"

rm -f $ia_logfile
check "(1) check ..-c ..-stop ..-local-bad-exit" --sshd2222-opts "$@" "$cmd -a -s --sshopts \"\" --check-mode-c --policy-stop --testcase-local-bad-exit; echo \$?; cat $ia_logfile" \
"$res"

rm -f $ia_logfile
check "(2) check ..-c ..-continue ..-local-bad-exit" --sshd2222-opts "$@" "$cmd -a -s --sshopts \"\" --check-mode-c --policy-continue --testcase-local-bad-exit; echo \$?; cat $ia_logfile" \
"$res"

rm -f $ia_logfile
check "(3) check ..-C ..-stop ..-local-bad-exit" --sshd2222-opts "$@" "$cmd -a -s --sshopts \"\" --check-mode-C --policy-stop --testcase-local-bad-exit; echo \$?; cat $ia_logfile" \
"$res"

rm -f $ia_logfile
check "(4) check ..-C ..-continue ..-local-bad-exit" --sshd2222-opts "$@" "$cmd -a -s --sshopts \"\" --check-mode-C --policy-continue --testcase-local-bad-exit; echo \$?; cat $ia_logfile" \
"$res"

rm -f $ia_logfile
