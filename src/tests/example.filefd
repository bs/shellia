#!/bin/sh
# vim: set filetype=sh :
#        file: example.filefd
#   copyright: Bernd Schumacher <bernd.schumacher@hpe.com> (2007-2021)
#     license: GNU General Public License, version 3
# description: test calling shellia script in separate file on local host
#    see also: test.filefd

set -e
set -u

. ./ia

eval "$ia_init"

filelog=""
file=""
while [ $# -gt 0 ]; do
  if [ "$1" = "--file" ]; then
    file="yes"
    shift
  elif [ "$1" = "--log" ]; then
    ia_check_logfile "$2"
    shift 2
  elif [ "$1" = "--filelog" ]; then
    file="yes"
    filelog="$2"
    shift 2
  else
    cat <<END >&2
Usage: example_filefd [--file] [--log <logfile>] [--filelog <file-logfile>]
  <logfile>       logs to logfile <logfile>
  <file-logfile>  implies --file and logs file to <file-logfile>
  --file          run separate file,
                  if only <logfile> is set, and not <file-logfile>
                  also file logs go to <logfile>
END
    exit 1
  fi
done

if [ "$file" ]; then

  testdir="$(realpath .)"

  ia_add "ia_log \"example_filefd-1: will run separate file\""
  ia_add "ia_logerr \"example_filefd-2: ia_logfile=<$<ia_logfile>> ia_log_fd=<$<ia_log_fd>>\""

  if [ "$filelog" ]; then
    ia_nocheck
    ia_add "ia_file $0 <-i> --log $filelog"
  else
    ia_nocheck
    ia_add "ia_file $0 <-i>"
  fi
else
  ia_add "ia_log \"example_filefd-3: running own script\""
  ia_add "ia_logerr \"example_filefd-4: ia_logfile=<$<ia_logfile>> ia_log_fd=<$<ia_log_fd>>\""
fi
ia -c
