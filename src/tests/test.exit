#!/bin/sh
# vim: set filetype=sh :
#        file: test.exit
#   copyright: Bernd Schumacher <bernd.schumacher@hpe.com> (2007-2020)
#     license: GNU General Public License, version 3
# description: test premature exit.
#              shellia will print everything that could not be checked before.
#    see also: example.exit

# The following tests are included:
# (1) check exit in ia -c user script
# (2) check logfile
# (3) check exit in ia -C user script
# (4) check logfile
# (5) check exit in ia -C user script
# (6) check logfile
# (7) check exit in ia -c user script with exit trap
# (8) check logfile
# (9) check exit in ia -C user script with exit trap
# (10) check logfile
# (11) check exit in ia user script with exit trap
# (12) check logfile

. ./tstlib

shell=""
while [ $# -ne 0 ]; do
  if [ "$1" = "-s" ]; then
    shell="$2"
    shift 2
  else
    echo "ERROR $0: unknown option <$1>" >&2
    exit 1
  fi
done

[ "$shell" ] && set -- "$@" -s "$shell"
cmd=$(dirname $0)/example.exit

ia_logfile="$(mktemp)"
export ia_logfile

rm -f $ia_logfile
check "(1) check exit in ia -c user script" "$@" "$cmd -a -s" \
"ERROR: ia premature exit of <usage>
unchecked output: <wrong usage error>
Command exited with non-zero status 1"

check "(2) check logfile" "$@" "cat $ia_logfile" \
"|example.exit"

rm -f $ia_logfile
cmd=$(dirname $0)/example.exit.tmp.ia-C
cat $(dirname $0)/example.exit | sed -e "s/-c/-C/" > $cmd
chmod 755 $cmd
check "(3) check exit in ia -C user script" "$@" "$cmd -a -s" \
"ERROR: ia premature exit of <usage>
unchecked output: <wrong usage error>
Command exited with non-zero status 1"

check "(4) check logfile" "$@" "cat $ia_logfile" \
"|example.exit.tmp.ia-C"

rm -f $ia_logfile
cmd=$(dirname $0)/example.exit.tmp.ia
cat $(dirname $0)/example.exit | sed -e "s/-c//" > $cmd
chmod 755 $cmd
check "(5) check exit in ia -C user script" "$@" "$cmd -a -s" \
"wrong usage error
Command exited with non-zero status 1"

check "(6) check logfile" "$@" "cat $ia_logfile" \
"|example.exit.tmp.ia"

rm -f $ia_logfile
cmd=$(dirname $0)/example.exit.tmp.ia-c.mytrap
cat $(dirname $0)/example.exit | sed -e \
  "/eval/aia_add \"trap \\\\\\\"echo \\\\\\\\\\\\\\\"my exit trap\\\\\\\\\\\\\\\"\\\\\\\" EXIT\\\"" \
  >$cmd
chmod 755 $cmd
check "(7) check exit in ia -c user script with exit trap" "$@" "$cmd -a -s" \
"ERROR: ia premature exit of <usage>
unchecked output: <wrong usage error>
my exit trap
Command exited with non-zero status 1"

check "(8) check logfile" "$@" "cat $ia_logfile" \
"|example.exit.tmp.ia-c.mytrap"

rm -f $ia_logfile
cmd=$(dirname $0)/example.exit.tmp.ia-C.mytrap
cat $(dirname $0)/example.exit | sed -e "s/-c/-C/" -e \
  "/eval/aia_add \"trap \\\\\\\"echo \\\\\\\\\\\\\\\"my exit trap\\\\\\\\\\\\\\\"\\\\\\\" EXIT\\\"" \
  >$cmd
chmod 755 $cmd
check "(9) check exit in ia -C user script with exit trap" "$@" "$cmd -a -s" \
"ERROR: ia premature exit of <usage>
unchecked output: <wrong usage error>
my exit trap
Command exited with non-zero status 1"

check "(10) check logfile" "$@" "cat $ia_logfile" \
"|example.exit.tmp.ia-C.mytrap"

rm -f $ia_logfile
cmd=$(dirname $0)/example.exit.tmp.ia.mytrap
cat $(dirname $0)/example.exit | sed -e "s/-c//" -e \
  "/eval/aia_add \"trap \\\\\\\"echo \\\\\\\\\\\\\\\"my exit trap\\\\\\\\\\\\\\\"\\\\\\\" EXIT\\\"" \
  >$cmd
chmod 755 $cmd
check "(11) check exit in ia user script with exit trap" "$@" "$cmd -a -s" \
"wrong usage error
my exit trap
Command exited with non-zero status 1"

check "(12) check logfile" "$@" "cat $ia_logfile" \
"|example.exit.tmp.ia.mytrap"

rm -f $ia_logfile
