#!/bin/sh
# vim: set filetype=sh :
#        file: test.ia-c-C
#   copyright: Bernd Schumacher <bernd.schumacher@hpe.com> (2007-2021)
#     license: GNU General Public License, version 3
# description: test using check normally but disable it for some functions
#    see also: example.ia-c-C

# The following tests are included:
# (1) Run this script without options

. ./tstlib

shell=""
while [ $# -ne 0 ]; do
  if [ "$1" = "-s" ]; then
    shell="$2"
    shift 2
  else
    echo "ERROR $0: unknown option <$1>" >&2
    exit 1
  fi
done

[ "$shell" ] && set -- "$@" -s "$shell"
cmd=$(dirname $0)/example.ia-c-C

ia_logfile="$(mktemp)"
export ia_logfile

rm -f $ia_logfile
check "(1) Interactively run step 1 and exit" -i "echo \"1
q
\"" "$@" "$cmd -a -i --" "=== example.ia-c-C ===
1 f_r
2 f_R
d ... change dbg: 0
c continue without questions
q quit
? [1] 
--- output with ERROR from <example.ia-c-C/f_r> running <f> ---
e:1.err
e:2.out
e:3.err
e:4.out
e:5.err
e:6.out
e:7.err
e:8.out
e:9.err
e:10.out
--- (q)uit (r)edo (i)gnore (s)witch interactive ---
? 
Command exited with non-zero status 1"

rm -f $ia_logfile
check "(2) Interactively run step 2 and exit" -i "echo \"2
q
\"" "$@" "$cmd -a -i" "=== example.ia-c-C ===
1 f_r
2 f_R
d ... change dbg: 0
c continue without questions
q quit
? [1] 
--- output with ERROR from <example.ia-c-C/f_R> running <f> ---
e:1.err
e:3.err
e:5.err
e:7.err
e:9.err
s:2.out
s:4.out
s:6.out
s:8.out
s:10.out
--- (q)uit (r)edo (i)gnore (s)witch interactive ---
? 
Command exited with non-zero status 1"

rm -f $ia_logfile
