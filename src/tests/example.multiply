#!/bin/sh
# vim: set filetype=sh :
#        file: example.multiply
#   copyright: Bernd Schumacher <bernd.schumacher@hpe.com> (2007-2020)
#     license: GNU General Public License, version 3
# description: example script that changes global and local variables in functions
#     warning: do not use this as an example, because iashell is not designed
#              for small functions. To learn more please read shellia(7)
#    see also: test.multiply
set -e
set -u

# this example is just to test some features of ia
# but ia should not be used in that way in real scripts

. ./ia

plus()
{
  eval "$ia_init"
  ia_stdout "^[0-9]+$"
  ia_add "echo $(($1 + $2))"
  ia -c
}

mult()
{
  local ret

  eval "$ia_init"
  ia_add "ret=0"
  for i in $(seq $1); do
    ia_nocheck
    ia_add "ret=\$(plus <-i> \$ret $2)"
  done
  ia_stdout "^[0-9]+$"
  ia_add "echo \$ret"

  ia -c
}

eval "$ia_init"
ia_nocheck
ia_add "x=\$(mult <-i> 3 2)"
ia_stdout "^x=[0-9]+$"
ia_add "echo x=\$x"
ia -c
