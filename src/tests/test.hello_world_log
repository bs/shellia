#!/bin/sh
# vim: set filetype=sh :
#        file: test.hello_world_log
#   copyright: Bernd Schumacher <bernd.schumacher@hpe.com> (2007-2021)
#     license: GNU General Public License, version 3
# description: test "hello world" script with logging
#    see also: hello_world_log

# The following tests are included:
# (1) Run with options -a -d 99 -- "shellia user"
# (2) Run with options -a -i "shellia user" and enter 2, c and q

. ./tstlib

shell=""
while [ $# -ne 0 ]; do
  if [ "$1" = "-s" ]; then
    shell="$2"
    shift 2
  else
    /bin/echo "ERROR $0: unknown option <$1>" >&2
    exit 1
  fi
done

[ "$shell" ] && set -- "$@" -s "$shell"

cmd="$(dirname $0)/hello_world_log"
use_locallib

check "(1) Run with options -d 99 -- \"shellia user\"" "$@" "$cmd -a -d 99 -- \"shellia user\"" \
"DEBUG main program begin
DEBUG function say_hello called with 1 arguments
hello shellia user
DEBUG function say_hello end
DEBUG main program end
--- LOGFILE ---
|hello_world_log.tmp.locallib
|DEBUG main program begin
||hello_world_log.tmp.locallib/say_hello
||DEBUG function say_hello called with 1 arguments
||s:hello shellia user
||DEBUG function say_hello end
|DEBUG main program end
---------------"

check "(2) Run hello_world interactive" -i "/bin/echo -e \"2\nc\nq\n\"" "$@" "$cmd -a -i \"shellia user\"" \
"=== hello_world_log.tmp.locallib ===
1 dbg \"main program begin\"
2 say_hello -i -- \"shellia user\"
3 dbg \"main program end\"
d ... change dbg: 0
i toggle -i flag
c continue without questions
q quit
? [1] 
=== hello_world_log.tmp.locallib/say_hello ===
1 dbg \"function say_hello called with 1 arguments\"
2 name=\"shellia user\"
3 echo \"hello \$name\"
4 dbg \"function say_hello end\"
d ... change dbg: 0
c continue without questions
q quit
? [1] 
hello shellia user
=== hello_world_log.tmp.locallib ===
1 dbg \"main program begin\"
2 say_hello -i -- \"shellia user\"
3 dbg \"main program end\"
d ... change dbg: 0
i toggle -i flag
q quit
? [3] 
--- LOGFILE ---
|=== hello_world_log.tmp.locallib ===
|1 dbg \"main program begin\"
|2 say_hello -i -- \"shellia user\"
|3 dbg \"main program end\"
|d ... change dbg: 0
|i toggle -i flag
|c continue without questions
|q quit
|? [1] 2<RETURN>
||=== hello_world_log.tmp.locallib/say_hello ===
||1 dbg \"function say_hello called with 1 arguments\"
||2 name=\"shellia user\"
||3 echo \"hello \$name\"
||4 dbg \"function say_hello end\"
||d ... change dbg: 0
||c continue without questions
||q quit
||? [1] c<RETURN>
||s:hello shellia user
|=== hello_world_log.tmp.locallib ===
|1 dbg \"main program begin\"
|2 say_hello -i -- \"shellia user\"
|3 dbg \"main program end\"
|d ... change dbg: 0
|i toggle -i flag
|q quit
|? [3] q<RETURN>
---------------"
