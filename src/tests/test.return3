#!/bin/sh
# vim: set filetype=sh :
#        file: test.return3
#   copyright: Bernd Schumacher <bernd.schumacher@hpe.com> (2007-2021)
#     license: GNU General Public License, version 3
# description: If no checking is done, because checking is expected to be done from a subfunction,
#              we have to exit if the subroutine is not callable.
#    see also: example.return3

# The following tests are included:
# (1) Run this script without options
# (2) check logfile

. ./tstlib

quote="'"
shell=""
while [ $# -ne 0 ]; do
  if [ "$1" = "-s" ]; then
    shell="$2"
    shift 2
  else
    echo "ERROR $0: unknown option <$1>" >&2
    exit 1
  fi
done

[ "$shell" ] && set -- "$@" -s "$shell" -u "$SED_command_not_found"
cmd=$(dirname $0)/example.return3

ia_logfile="$(mktemp)"
export ia_logfile

rm -f $ia_logfile
check "(1) Run this script without options" -i "/bin/echo \"i
\"" "$@" "$cmd" \
": not: not found
--- output with ERROR from <example.return3/fun1> running <not existing command> ---
e:[0;31mexit=127[0m
--- (q)uit (r)edo (i)gnore (s)witch interactive ---
? 
fun1 end
: not: not found
fun1 end
main end"

check "(2) check logfile" "$@" "cat $ia_logfile" \
"|example.return3
||example.return3/fun1
||--- output with ERROR from <example.return3/fun1> running <not existing command> ---
||e:[0;31mexit=127[0m
||--- (q)uit (r)edo (i)gnore (s)witch interactive ---
||? i<RETURN>
||s:fun1 end
||example.return3/fun2
||s:fun1 end
|s:main end"

rm -f $ia_logfile

