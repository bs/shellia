#!/bin/sh
# vim: set filetype=sh :
#        file: example.v
#   copyright: Bernd Schumacher <bernd.schumacher@hpe.com> (2007-2022)
#     license: GNU General Public License, version 3
# description: check pressing of v to show \${variable}
#    see also: test.v
#
# check correct output of " in interactive menu and in logfile
#
#
set -e
set -u

. ./ia

eval "$ia_init"
ia_add "xyz=39"
ia_add "xyz=\$(( $<xyz> + 1 ))"
ia_add "xyz=\$(( $<xyz> + 1 ))"
ia_add "xyz=\$(( $<xyz> + 1 ))"
ia_stdout "^xyz=[0-9]+$"
ia_add "/bin/echo \"xyz=$<xyz>\""
ia_stdout "^xyz=[0-9]+meter backslash=\\\\ quote=\\\" dollar=\\\$ dollarX=\\\$X$"
ia_add "/bin/echo \"xyz=$<xyz>meter backslash=\\\\\\\\ quote=\\\" dollar=\\\$ dollarX=\\\$X\""
ia_add "a=\"
---
\""
ia_add "/bin/echo \"prg \\\"$<a>\\\"\""



ia #-c
