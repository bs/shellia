all	: shellia_*.deb

shellia_*.deb	: \
	Makefile \
	src/ia \
    src/ia.array \
    src/ia.basic \
	src/ia.check \
	src/ia.debug \
	src/ia.interactive \
	src/ia.log \
	src/shellia-1.rst \
	src/shellia-3.rst \
	src/shellia-7.rst \
	src/Makefile \
	src/debian/rules
	@if [ $(USER) = "root" ]; then echo "Makefile: do not run as root"; exit 1; fi
	@#(cd src; debuild -- clean binary)
	@(cd src; debuild --set-envvar=NOTEST=$(NOTEST) $(USUC) --lintian-opts -i -- binary)

clean	:
	@(cd src; debuild -- clean)
	@rm -f shellia_*.dsc
	@rm -f shellia_*.tar.gz
	@rm -f shellia_*.tar.xz
	@rm -f shellia_*.build
	@rm -f shellia_*.buildinfo
	@rm -f shellia_*.changes

clobber : clean
	@rm -f shellia_*.deb
	@rm -f src/shellia-tested-stamp

install	: /var/lib/dpkg/info/shellia.list

notest: NOTEST=True
notest: shellia_*.deb

nosign: USUC=-us -uc
nosign: shellia_*.deb

/var/lib/dpkg/info/shellia.list: shellia_*.deb
	@sudo dpkg --force-confmiss -i shellia_*.deb
